import datetime

MAX_CACHE_SIZE = 100

global _cache


def init():
    """Initialises cache"""
    global _cache
    _cache = {}  # maps key to (datetime, value) tuple


def set_cache(key, value):
    """Setting of key and value in cache"""
    global _cache
    if key not in _cache and len(_cache) >= MAX_CACHE_SIZE:
        _remove_oldest_entry()  # _ means function is also private
    _cache[key] = [datetime.datetime.now(), value]


def get_cache(key):
    """Retrieves key and value in cache"""
    global _cache
    if key in _cache:
        _cache[key][0] = datetime.datetime.now()  # we update the date and time again to current time
        # so we know when entry was last used
        return _cache[key][1]  # retrieve value
    else:
        return None


def contains(key):
    """Returns the keys in the cache"""
    global _cache
    return key in _cache


def size():
    """Returns the number of items in the cache"""
    global _cache
    return len(_cache)


def _remove_oldest_entry():
    """The first time the program goes through its first iteration, oldest is set to None. But as it goes
     through its second iteration in the for loop it compares the datetime value of oldest against that of the
     next iteration to see which is lesser. An older time has a lesser value than a recent time. A lesser time is
     set as the oldest and that process goes on till iteration is complete. Oldest entry is deleted.
     NB: Dicts are not ordered by default so we used a for loop to iterate for oldest value chronologically.
     A list or other data structure won't need such handiwork since we would just delete the first item in the list or
     said data structure unless data structure uses hash.
    """
    global _cache
    oldest = None
    for key in _cache.keys():
        if oldest is None:
            oldest = key
        elif _cache[key][0] < _cache[oldest][0]:
            oldest = key
    if oldest is not None:
        del _cache[oldest]

